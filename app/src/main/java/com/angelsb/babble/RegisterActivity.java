package com.angelsb.babble;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends AppCompatActivity {

    private EditText mDisplayNameField;
    private EditText mEmailField;
    private EditText mPasswordField;

    private Button mSignupBtn;

    private FirebaseAuth mAuth;

    private DatabaseReference mDatabase;

    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mProgress = new ProgressDialog(this);

        mAuth  = FirebaseAuth.getInstance();

        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDisplayNameField = findViewById(R.id.displaynamefield_id);
        mEmailField = findViewById(R.id.emailfield_id);
        mPasswordField = findViewById(R.id.passwordfield_id);

        mSignupBtn = findViewById(R.id.signupbtn_id);
        mSignupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                createAccount();

            }
        });


    }

    private void createAccount() {

        final String displayName = mDisplayNameField.getText().toString().trim();
        final String email = mEmailField.getText().toString().trim();
        final String password = mPasswordField.getText().toString().trim();

        if( !TextUtils.isEmpty(displayName) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(password) ){

            mProgress.setMessage("Signing up..");
            mProgress.show();

            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                FirebaseUser user = mAuth.getCurrentUser();

                                Toast.makeText(RegisterActivity.this, "Successfully Signed up",
                                        Toast.LENGTH_SHORT).show();
                                updateUI(user, displayName);
                            } else {
                                // If sign in fails, display a message to the user.
                                Toast.makeText(RegisterActivity.this, "Registration failed. "+task.getException().getMessage(),
                                        Toast.LENGTH_LONG).show();
                                updateUI(null, null);
                            }

                            // ...
                        }
                    });

        }

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        redirectHome(currentUser);
    }

    private void updateUI(FirebaseUser currentUser, String displayName) {

        if (currentUser != null) {

            // Name, email address, and profile photo Url
            String name = currentUser.getDisplayName();
            String email = currentUser.getEmail();
            Uri photoUrl = currentUser.getPhotoUrl();

            // Check if user's email is verified
            boolean emailVerified = currentUser.isEmailVerified();

            // The user's ID, unique to the Firebase project. Do NOT use this value to
            // authenticate with your backend server, if you have one. Use
            // FirebaseUser.getToken() instead.
            String userId = currentUser.getUid();


            User user = new User(displayName, email, "default");

            mDatabase.child("Users").child(userId).child("displayname").setValue(user.displayname);
            mDatabase.child("Users").child(userId).child("email").setValue(user.email);
            mDatabase.child("Users").child(userId).child("imageurl").setValue(user.imageurl);

            mProgress.dismiss();

            Intent homeIntent = new Intent(RegisterActivity.this, HomeActivity.class);
            startActivity(homeIntent);


        }

    }

    private void redirectHome(FirebaseUser currentUser) {

        if (currentUser != null) {
            Intent homeIntent = new Intent(RegisterActivity.this, HomeActivity.class);
            startActivity(homeIntent);
        }

    }
}
