package com.angelsb.babble;

/**
 * Created by bals on 1/1/2018.
 */

public class User {

    public String displayname;
    public String email;
    public String imageurl;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String username, String email, String imageurl) {
        this.displayname = username;
        this.email = email;
        this.imageurl = imageurl;
    }

}
